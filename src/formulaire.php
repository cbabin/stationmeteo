<?php
require('./functions/functionsindex.php');
require('./functions/functionsform.php');
require('./../datas/datas.php');
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" media="screen" type="text/css" href="css/style.css" />
    <title>Formulaire</title>
</head>
<body>
  <div class="formulaireweather center">
  <form action="validation.php" method="post" enctype="multipart/form-data">
    <div>
      <label for="town">Renseignez une ville: <br/></label>
      <input type="text" id="town" name="town">
    </div>
    <div>
      <label for="Region">Choississez la region</label><br />
      <select name="regionchoose" id="regionchoose">
          <?php regionform($readcsv) ?>
      </select>
      </label>
    </div>
    <div>
      <label for="temperature">Température: <br/></label>
      <input type="text" id="temperature" name="temperature" >
    </div>
    <div>
    <label for="Weather">Choississez le temps</label><br />
      <select name="weatherchoose" id="weatherchoose">
          <?php weatherform($readcsv) ?>
      </select>
      </label>
    </div>
    <div class="button">
      <button type="submit">Envoyer</button>
    </div>  
  </form>
</div>
</body>
</html>