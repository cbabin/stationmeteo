<?php
require('./functions/functionsindex.php');
require('./../datas/datas.php');
read($csv);

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" media="screen" type="text/css" href="css/style.css" />
    <title>Centrale Meteo</title>
</head>
<body>
<div class= "title center">
<h1>Station Météo</h1>
<!--
<form action="" method="post" action="displaychoose">
   <p>
       <label for="displaychoose">Choississez les options d'apparitions</label><br />
       <select name="displaychoose" id="displaychoose">
           <option value="croissant">Afficher par ordre croissant de température</option>
           <option value="decroissant">Afficher par ordre décroissant de température</option>
           <option value="warning">Afficher seulement les régions en vigilance orange/rouge</option>           
        </select>

        <div class="button">
            <button type="submit">Envoyer</button>
        </div>
    </p>
</form>
-->
<form action="" method="post">
        <div>
            <button value="--00" name="choosetemp">En dessous de 0°C</button>
            <button value="0005" name="choosetemp">Entre 0 et 5°C</button>
            <button value="0510" name="choosetemp">Entre 5 et 10°C</button>
            <button value="1015" name="choosetemp">Entre 10 et 15°C</button>
            <button value="1520" name="choosetemp">Entre 15 et 20°C</button>
            <button value="2025" name="choosetemp">Entre 20 et 25°C</button>
            <button value="2530" name="choosetemp">Entre 25 et 30°C</button>
            <button value="3099" name="choosetemp">Au dela de 30°C</button>
        </div>
</form>
</div>
<div class = "flex black">
<?php
/*
if ($_POST['displaychoose'] == "croissant"){
    print_r ($line);
}
elseif ($_POST['displaychoose'] == "decroissant"){
    echo "decroissant";
}
elseif ($_POST['displaychoose'] == "warning"){
    echo "warning";
}
*/
if (isset($_GET['region'])) {
    displaytown($readtemptown);
}
else {
display($readcsv);
}

?>
</div>
<div class="addmore center">
    <a href="formulaire.php" class="white">Ajouter un relevé personnalisée</a>
    <br/>
    <a href="index.php" class="white">Revenir en arrière</a>
</div>
</body>
</html>
