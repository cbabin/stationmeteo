<?php
require('./functions/functionsindex.php');
require('./../datas/datas.php');
?>
<h1>validation</h1>
<h4>Affichage de $_FILES</h4>
<table border=1>
<thead>
<tr>
    <th>Champs</th><th>Valeur</th><th>Est Alpha ?</th><th>Est numérique ?</th>
    <th>Email ? </th><th>Int ? </th><th>Float ?</th><th>Mail autorisé ?</th>
</tr>
</thead>
<?php
$currentkey = $_POST;
foreach($currentkey as $key => $value):?>
<tr>
    <td><?= $key ?>
    <td><?= $value ?>
</style>
    <td><?= ctype_alpha($value) ? 'oui':'non'; ?></td>
    <td><?= ctype_digit($value) ? 'oui':'non'; ?></td>
    <td><?= filter_input(INPUT_POST,$key,FILTER_VALIDATE_EMAIL) ? "oui":"non"; ?></td>
    <td><?= filter_input(INPUT_POST,$key,FILTER_VALIDATE_INT) ? "oui":"non"; ?></td>
    <td><?= filter_input(INPUT_POST,$key,FILTER_VALIDATE_FLOAT) ? "oui":"non"; ?></td>
    <td>
            <?php
             if(filter_input(INPUT_POST,$key,FILTER_VALIDATE_EMAIL))
             {
                if(preg_match("/com|nze|fr$/",$value,$tm)){
               
               echo "email autorisé";
                }
            else {
                echo "email non autorisé";
                }
            }
            ?>
        </td>
    </tr>
</tr>
<?php
endforeach;
?>
</table>
<?php
// La vérification de l'entrée ville
$verification = "/[0-9]/";
if (empty($_POST['town'])){
    array_push ($errorfound, "Le nom de la ville est vide");
}
else if (preg_match($verification, $_POST['town']) == 1) {
    array_push ($errorfound, "Le nom de la ville contient des caractères indésirables");
}
else {
    $validationok ++;
    $town = $_POST['town'];
}
// La vérification de la valeur à virgule
if (empty($_POST['temperature'])) {
    array_push ($errorfound, "La température est vide");
    }
else if (filter_var($_POST['temperature'], FILTER_VALIDATE_FLOAT)) {
    $validationok ++;
    $temp = $_POST['temperature'];
}
else {
    array_push ($errorfound, "La valeur renseignée n'est pas un chiffre flottant, merci de remplacer la ',' par un point .");
}
$region = $_POST['regionchoose'];
$weather = $_POST['weatherchoose'];
echo "<div style='text-align:center;'>";
if ($validationok == 2){
    $temporaire = [$town,$region,$temp,$weather];
    var_dump($temporaire);
    $file = fopen('./../datas/temptown.csv', 'a');
    fputcsv($file, $temporaire, ";");
    fclose('./../datas/temptown.csv');
    header("Location:index.php");
}
else {
    foreach ($errorfound as $key => $value){
        if ($value !== 0){
        echo $value . "<br/>";
        }
    }
}
echo "<a href='formulaire.php'>Retour au formulaire</a><br/>"; 
echo "<a href='index.php'>Retour à l'index</a>"; 
echo "</div>";
?>